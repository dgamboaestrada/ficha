import React, {Component} from 'react';

class Discover extends Component {
render() {
    return (
      <div>
        <div className="paragraph-style">
          <img className="img-discover" src="../../assets/images/temporary/work-sheets/statues.png" />
          <p>La intención de esta experiencia consiste en resolver con álgebra un enigma vinculado a un hecho histórico y artístico que forma parte de la cultura universal.</p>
          <span>
            <p>El emperador chino Qin Shi Huang estaba tan obsesionado con la idea de morir que prohibía a sus allegados pronunciar la palabra muerte
              y al mismo tiempo orden. La creación de un ejército que lo acompañara en su viaje al más allá; se hicieron entonces miles de ﬁguras en
              terracota, de tamaño natural y todos con distinto rostro, incluso pertenecientes a diferentes etnias, de manera que no hay dos figuras
              iguales. Hace poco trompo se encontró un pergamino casi intacto en el que se lee lo que alguno de sus consejeros le dijo al emperador:<br/>
              t00 que no lo saben.</p>
            <p>El emperador Qin, irritado, le dijo:</p>
            <p>- Te pregunto cuántos guerreros tengo, no que saben o ignoran.</p>
            <p>- Ya se lo he dicho, emperador -dijo contundente su consejero.</p>
          </span>
        </div>
        <div className="grey-square">
          <h3>Información adicional:</h3>
          <p>En el año 221 a. C. se usó la forma huangdi para referirse a este primer emperador chino (huang significa "rey dios" y Di, "rey sabio") de la dinastía Qin, que terminó en 1911. Finalmente, dicho término se abrevió como "Huang" o "Di", perdiendo cada una de las partes su significado original.</p>
          <h3>Vínculo con:</h3>
          <p>Historia Universal, Geografía, Formación Cívica y Ética.</p>
          <h3>Emoción:</h3>
          <p>Pregunte lo siguiente:<br/>¿Puedes distinguir alguna emoción al leer este texto y ver la imagen? ¿Podrías describirla? Ayude a los alumnos a hacer contacto con sus emociones y expresarlas por escrito.</p>
          <p>Este texto y su imagen pretenden generar asombro, admiración o sorpresa, pero es posible que cada estudiante reconozca una emoción distinta dependiendo de sus aprendizajes y experiencias previas.</p>
        </div>
        <div className="reflection">
          <h1>
            <img src="/assets/images/temporary/sheet1.png" />
            <span>¿Qué emociones sintieron<br/>tus alumnos?</span>
          </h1>
          <table className="table-valuations table-padding">
            <thead>
              <tr>
                <th>Emoción</th>
                <th>Promedio en el grupo</th>
                <th>Detalle</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Alegría</td>
                <td>
                  <div className="percentage">
                    <div>6 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '20%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Miedo</td>
                <td>
                  <div className="percentage">
                    <div>2 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '6.6%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Tristeza</td>
                <td>
                  <div className="percentage">
                    <div>8 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '26.6%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Sorpresa</td>
                <td>
                  <div className="percentage">
                    <div>3 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '10%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Disgusto</td>
                <td>
                  <div className="percentage">
                    <div>6 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '20%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Otro</td>
                <td>
                  <div className="percentage">
                    <div>5 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '16.6%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default Discover;
