import React,{Component} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import classNames from 'classnames';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

class Create extends Component {
  constructor(props) {
    super(props);
    this.goActivity = this.goActivity.bind(this);
  }
  goActivity() {
    browserHistory.push('/teachers/terracota');
  }
  render() {
    return (
      <div>
        <div className="paragraph-style">
          <p>-A partir de sus resultados, elaboren un modelo que represente la cantidad de guerreros que conformaban el ejército; para ello, elijan una de las siguientes opciones para completar el enunciado que aparece a continuación:</p>
          <p>El consejero dio a entender al emperador que había<br/>a. 4400 guerreros.<br/>b. 1100 guerreros.<br/>c. 2200 guerreros.<br/>d. 220 guerreros.</p>
          <p>-¿Cuá de las opciones es correcta?<br/>-¿Crees haber resuelto el enigma de los Guerreros de terracota?<br/>-¿Podrían aplicar lo que han hecho a otros casos que les interesen?</p>
        </div>
        <div className="grey-square">
          <h3 className="title-margin">Crea</h3>
          <p>Este es un espacio para que los alumnos elijan libremente, y por concenso entre ellos, las herramientas y materiales que utilizarán para exponer sus ideas y compartirlas en su portafolio de experiencias.</p>
        </div>
        <div className="text-right">
          <RaisedButton
            target="_blank"
            label="Enviar a Classroom"
            icon={<i className="fa fa-users" style={{color: '#ffffff'}}></i>}
            style={{marginTop: '20px'}}
            backgroundColor="#008a50"
            labelColor="#ffffff"
            />
        </div>
        <div>
          <div className="rectangle-style bar-green" id="crea" style={{backgroundColor: '#764AFF'}}>
            <div className="bar-txt">
              Actividad
            </div>
            <div className="toggle-info">
              <i
              className={classNames('fa', 'bar-icon', 'fa-caret-down')}
              aria-hidden="true"
              >
              </i>
            </div>
          </div>
          <div className="student-main-activity">
            <div className="row">
              <div className="col-sm-5">
                <CardMedia>
                  <iframe width="100%" height="230" src="https://www.youtube.com/embed/NaFd_tFbpXE" frameBorder="0" allowFullScreen></iframe>
                </CardMedia>
              </div>
              <div className="col-sm-7">
                <div className="activity-description">
                  <h2>Documental Los Soldados de Terracota</h2>
                  <h3>Descripción:</h3>
                  <p>Te invitamos a conocer más sobre los guerreros de terracota. Después del ver el video (investigación) escribe un ensayo donde expliques dónde se encuentran y porqué se consideran patrimonio de la humanidad. </p>
                  <h4>Más información:</h4>
                  <p>Conoce más sobre estos personajes en estas ligas: </p>
                  <p><a href="https://www.youtube.com/watch?v=NaFd_tFbpXE">https://www.youtube.com/watch?v=NaFd_tFbpXE</a></p>
                  <p><a href="https://www.youtube.com/watch?v=RsUE-ZtcUFg">https://www.youtube.com/watch?v=RsUE-ZtcUFg</a></p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-5">
                <p>Los Soldados de Terracota.</p>
              </div>
              <div className="col-sm-7">
                <div className="activity-description">
                  <div className="row">
                    <div className="col-sm-6">
                      <RaisedButton
                        label="Agrega más información"
                        backgroundColor={'##9E9E9E'}
                        labelColor={'#fff'}
                        fullWidth={true}
                        labelStyle={{fontWeight: '400'}}
                      />
                    </div>
                    <div className="col-sm-6">
                      <RaisedButton
                        label="Ir a la actividad"
                        backgroundColor={'#283593'}
                        labelColor={'#fff'}
                        fullWidth={true}
                        labelStyle={{fontWeight: '400'}}
                        onClick={this.goActivity}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="reflection">
          <h1>
            <img src="/assets/images/temporary/sheet1.png" />
            <span>¿Qué emociones sintieron<br/>tus alumnos?</span>
          </h1>
          <table className="table-valuations table-padding">
            <thead>
              <tr>
                <th>Emoción</th>
                <th>Promedio en el grupo</th>
                <th>Detalle</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Alegría</td>
                <td>
                  <div className="percentage">
                    <div>10 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '33.3%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Miedo</td>
                <td>
                  <div className="percentage">
                    <div>2 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '6.6%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Tristeza</td>
                <td>
                  <div className="percentage">
                    <div>3 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '10%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Sorpresa</td>
                <td>
                  <div className="percentage">
                    <div>3 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '10%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Disgusto</td>
                <td>
                  <div className="percentage">
                    <div>3 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '10%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
              <tr>
                <td>Otro</td>
                <td>
                  <div className="percentage">
                    <div>9 de 30</div>
                    <div>
                      <div className="progress-bar">
                        <div className="progress" style={{width: '30%'}}></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <i className="fa fa-eye" aria-hidden="true"></i>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
export default Create;
