import React,{Component} from 'react';

class Habilities extends Component {
  render() {
    return (
      <div className="reflection">
        <table className="table-valuations table-padding">
          <thead>
            <tr>
              <th>Emoción</th>
              <th>Promedio en el grupo</th>
              <th>Detalle</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Comunicación</td>
              <td>
                <div className="percentage">
                  <div>22 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '73.3%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Colaboración</td>
              <td>
                <div className="percentage">
                  <div>23 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '76.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Creatividad</td>
              <td>
                <div className="percentage">
                  <div>8 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '26.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Resolución de<br/>Problemas</td>
              <td>
                <div className="percentage">
                  <div>26 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '86.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Manejo de<br/>información</td>
              <td>
                <div className="percentage">
                  <div>11 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '36.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Uso de medios</td>
              <td>
                <div className="percentage">
                  <div>8 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '26.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Manejo de<br/>tecnología</td>
              <td>
                <div className="percentage">
                  <div>15 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '50%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Curiosidad</td>
              <td>
                <div className="percentage">
                  <div>17 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '56.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Iniciativa</td>
              <td>
                <div className="percentage">
                  <div>8 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '26.6%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
            <tr>
              <td>Persistencia</td>
              <td>
                <div className="percentage">
                  <div>25 de 30</div>
                  <div>
                    <div className="progress-bar">
                      <div className="progress" style={{width: '83.3%'}}></div>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default Habilities;
