import React,{Component} from 'react';

class SimpleVideo extends Component {
  render() {
    return (
      <div className="video-config">
        <iframe className="video-position" width="80%" height="500" src="https://www.youtube.com/embed/_mDlhV9fJWI" frameBorder="0" allowFullScreen></iframe>
      </div>
    );
  }
}

export default SimpleVideo;
