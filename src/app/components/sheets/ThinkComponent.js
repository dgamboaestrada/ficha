import React, {Component} from 'react';

class Think extends Component {
  render() {
    return (
      <div>
        <div className="paragraph-style">
          <p>-¿Cómo se podría resolver el "enigma de los Guerreros de terracota"?, ¿qué estrategia utilizarías?</p>
          <p>-Platica con tu pareja o con tu equipo y pregúntales qué estrategia eligieron.</p>
          <p>-¿Te sirve comparar otras estrategias con las tuyas?, ¿por qué?</p>
          <p>-Elijan la que consideren más útil.</p>
          <p>-Una manera de resolver  este enigma  es plantear una ecuación que nos ayude a calcular el número de guerreros. De las siguientes ecuaciones, elige la que equivalga a lo que el consejero le dio a entender al emperador:</p>
          <img src="/assets/images/temporary/work-sheets/equation1.png" />
          <p>-Complementen la historia: investiguen quiénes fueron los Guerreros de terracota, dónde se encuentran y por qué se les considera patrimonio de la humanidad.</p>
        </div>
        <div className="grey-square">
          <h3 className="title-margin">Idea</h3>
          <p><strong>a.</strong> Respuesta correcta.</p>
          <p><strong>b.</strong> El consejero dio a entender al emperador que había x guerreros, donde <span><img src="/assets/images/temporary/work-sheets/equation2.png" /></span><br/>Esta opción la escogerán aquellos que ignoren la información sobre los 1,100 guerreros que se incluyen en el texto. Hay que notar que esta ecuación no tiene solución</p>
          <p><strong>c.</strong> El consejero dio a entender al emperador que había x guerreros, donde x = 100 + 1000.<br/>Esta opción la elegirán los que no tomen en cuenta la información que se da sobre las fracciones del total.</p>
          <p>El consejero dio a entender al emperador que había x guerreros, donde <span><img src="/assets/images/temporary/work-sheets/equation3.png" /></span></p>
        </div>
      </div>
    );
  }
}

export default Think;
