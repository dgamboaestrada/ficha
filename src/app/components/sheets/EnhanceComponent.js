import React,{Component} from 'react';

export class Enhance extends Component {
  render() {
    return (
      <div>
        <div>
          <h4>MED RELACIONADOS</h4>
        </div>
        <table className="table-calendar table-border">
          <thead>
            <tr>
              <th className="th-border">Fichas relacionadas</th>
              <th>Planeaciones relacionadas</th>
              <th>Meds relacionas</th>
              <th>Evaluaciones sugeridas disponibles</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <div className="meds-set">
                  <div>
                    <div className="img-relative">
                      <img src="/assets/images/temporary/calendar/cal1.png" />
                      <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                    </div>
                  </div>
                  <div>
                    <p className="title">Lengua Castellana y poemas del s...</p>
                    <p>Duración 2 días</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal6.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Conocer una cancion</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal10.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Día Mundial de la Población</p>
                  </div>
                </div>
              </td>
              <td>
                <a href='#'>4 reactivos</a>
              </td>
            </tr>
            <tr>
              <td>
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal2.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p className="title">Albúm de los recuerdos de la..</p>
                    <p>Duración 4 días</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal7.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Las diferentes manifestaciones culturales</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal11.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>¿En qué se parece Shakespeare y</p>
                  </div>
                </div>
              </td>
              <td>
                <a href='#'>5 reactivos</a>
              </td>
            </tr>
            <tr>
              <td>
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal3.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p className="title">La biblioteca y yo</p>
                    <p>Duración 2 dias</p>
                  </div>
                </div>
                <div className="meds-set" style={{marginTop: '20px'}}>
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal4.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p className="title">Información para un futuro</p>
                    <p>Duración 2 dias</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal8.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Comunica ideas y sentimientos</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal12.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Verificador de Ortagrafía en Línea</p>
                  </div>
                </div>
              </td>
              <td>
                <a href='#'>4 reactivos</a>
              </td>
            </tr>
            <tr>
              <td>
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal5.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p className="title">¿Qué es la jerarquía?</p>
                    <p>Duración 1 día</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal9.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Conocer los datos biográficos</p>
                  </div>
                </div>
              </td>
              <td className="title">
                <div className="meds-set">
                  <div className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal13.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Lengua Castellana y literatura</p>
                  </div>
                </div>
              </td>
              <td>
                <a href='#'>6 reactivos</a>
              </td>
            </tr>
          </tbody>
        </table>
        <div>
          <h4>MED RELACIONADOS</h4>
        </div>
        <table className="table-med-rel">
          <thead>
            <tr>
              <th className="th-border">Título</th>
              <th>Descripción</th>
              <th >Ver</th>
              <th>Agregar</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal10.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Día Mundial de la Población</p>
                  </div>
                </div>
              </td>
              <td>
                <p>Libro que contiene ejemplos de rubricas orientadas a evaluar diferentes productos.</p>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal11.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>¿En qué se parecen Shakespeare y Cervantes? O... El Día Mundial del</p>
                  </div>
                </div>
              </td>
              <td>
                <p>Se recomienda al docente que lea el documento para utilizar las rúbricas que contiene o generar las propias.</p>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal12.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Verificardor de Ortografía en Línea</p>
                  </div>
                </div>
              </td>
              <td>
                <p>Documento de Administración Federal de Servicios Educativos que presenta 20 juegos en que los estudiantes se diviertan mientras desarrollan.</p>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>
              <td className="title">
                <div className="meds-set">
                  <div  className="img-relative">
                    <img src="/assets/images/temporary/calendar/cal13.png" />
                    <img className="img-absolute" src="/assets/images/temporary/work-sheets/more.png" />
                  </div>
                  <div>
                    <p>Lengua Castellana y literatura</p>
                  </div>
                </div>
              </td>
              <td>
                <p>Este MED ayudará a orientar a los educadores y docentes de educación temprana para que ayuden a los niños a aprender de acuerdo a su propio estilo.</p>
              </td>
              <td>
                <i className="fa fa-eye" aria-hidden="true"></i>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
          </tbody>
        </table>
        <div>
          <h4>REACTIVOS RELACIONADOS</h4>
        </div>
        <table className="table-rea-rel">
          <thead>
            <tr>
              <th className="th-border">Reactivo</th>
              <th>Respuestas</th>
              <th>Agregar</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <p>Al efectuar la suma 5/3 + 8/6 se obtiene como resultado.</p>
              </td>
              <td>
                <p className="correct"><span>a)</span> 4/8</p>
                <p><span>b)</span> 12/24</p>
                <p><span>c)</span> 13/9</p>
                <p><span>d)</span> 3/24</p>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>
              <td>
                <p>Don Guillermo compró un automóvil que cuneta $93,580. Si dio un enganche de 15% ¿Cuánto le falta por pagar?.</p>
              </td>
              <td>
                <p><span>a)</span> 13-913</p>
                <p><span>b)</span> 14.038</p>
                <p className="correct"><span>c)</span> 79.543</p>
                <p><span>d)</span> 79.667</p>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
            <tr>
              <td>
                <p>En una fábrica se empacan latas de atún. Si en 100 cajas caben 1500 latas, ¿Cuántas se podrán acomodar en 350 cajas?.</p>
              </td>
              <td>
                <p className="correct"><span>a)</span>4/8</p>
                <p><span>b)</span>   12/24</p>
                <p><span>c)</span>   13/9</p>
                <p><span>d)</span>   3/24</p>
              </td>
              <td>
                <img src="/assets/images/temporary/work-sheets/more.png" />
              </td>
            </tr>
          </tbody>
        </table>
        <div>
          <h4>PLANEACIONES RELACIONADOS</h4>
        </div>
        <table className="table-pla-rel">
          <thead>
            <tr>
              <th>PLAN</th>
              <th>Aprendizaje</th>
              <th>Aprendizaje</th>
              <th>Ver</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="title">161</td>
              <td>Resolver un problema de forma autónoma</td>
              <td>Resuelve problema que implican el cálculo mental o escrito de productos digitos</td>
              <td><i className="fa fa-eye" aria-hidden="true"></i></td>
            </tr>
            <tr>
              <td className="title">162</td>
              <td>Resolver un problema de forma autónoma</td>
              <td>Resuelve problema que implican el cálculo mental o escrito de productos digitos</td>
              <td><i className="fa fa-eye" aria-hidden="true"></i></td>
            </tr>
            <tr>
              <td className="title">163</td>
              <td>Uso de caminos cortos para multiplicar digitos por 10 o por sus múltiplos (20, 30, etcétera)</td>
              <td>Resuelve problema que implican el cálculo mental o escrito de productos digitos</td>
              <td><i className="fa fa-eye" aria-hidden="true"></i></td>
            </tr>
            <tr>
              <td className="title">164</td>
              <td>Resolver un problema de forma autónoma</td>
              <td>Resuelve problema que implican el cálculo mental o escrito de productos digitos</td>
              <td><i className="fa fa-eye" aria-hidden="true"></i></td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default Enhance;
