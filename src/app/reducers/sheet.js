import {
  FETCH_SHEET_PENDING,
  FETCH_SHEET_FULFILLED,
  FETCH_SHEET_REJECTED
} from '../constants/ActionTypes';

const initialState = {
  content: null,
  loading: false
};

export default function sheet(state = initialState, action) {
  switch (action.type) {
    case FETCH_SHEET_PENDING:
      return {
        ...state,
        loading: true
      };
    case FETCH_SHEET_FULFILLED:
      return {
        ...state,
        loading: false,
        content: action.payload.data
      }
    default:
      return state;
  }
}
