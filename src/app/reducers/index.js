import {combineReducers} from 'redux';
import todos from './todos';
import sheet from './sheet';
import sheets from './sheets';

const rootReducer = combineReducers({
  todos,
  sheet,
  sheets
});

export default rootReducer;
