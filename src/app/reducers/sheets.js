import {LOADING_SHEETS_FULFILLED, LOADING_SHEETS_REJECTED, LOADING_SHEETS_PENDING} from '../constants/ActionTypes';

const initialState = {
    items: [],
    loading: false
  };

export default function sheets(state = initialState, action) {
  switch (action.type) {
    case LOADING_SHEETS_FULFILLED:
      return {
        ...state,
        loading: false,
        content: action.payload.data
      };

    case LOADING_SHEETS_REJECTED:
      return {
        ...state,
        loading: false
      };

    case LOADING_SHEETS_PENDING:
      return {
        ...state,
        loading: true
      };

    default:
      return state;
  }
}
