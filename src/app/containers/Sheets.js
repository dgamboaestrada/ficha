import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Header from '../components/Header';
import MainSection from '../components/MainSection';
import * as TodoActions from '../actions/index';

class Sheets extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const {actions} = this.props;
    actions.fetchSheets();
  }

  render() {
    const {sheets, actions} = this.props;
    var nodos = sheets.items.map(function(sheet) {
      return (
          <span>{sheet.post_title}</span>
      );
    });

    return (
      <div>
        {
          sheets.loading ? (
            <spam>cargando...</spam>
            ) : {nodos}
        }
      </div>
    );
  }
}

Sheets.propTypes = {
  sheets: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    sheets: state.sheets
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sheets);
