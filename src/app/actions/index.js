import * as types from '../constants/ActionTypes';
import axios from 'axios';

export function addTodo(text) {
  return {type: types.ADD_TODO, text};
}

export function deleteTodo(id) {
  return {type: types.DELETE_TODO, id};
}

export function editTodo(id, text) {
  return {type: types.EDIT_TODO, id, text};
}

export function completeTodo(id) {
  return {type: types.COMPLETE_TODO, id};
}

export function completeAll() {
  return {type: types.COMPLETE_ALL};
}

export function clearCompleted() {
  return {type: types.CLEAR_COMPLETED};
}

export function fetchSheet() {
  return (dispatch, getState) => {
    dispatch({
      type: types.FETCH_SHEET,
      payload: axios.get('http://potencia.vincoorbisdev.com/wp-json/potencia/v1/files/62')
    });
  };
}

export function fetchSheets() {
  return (dispatch, getState) => {
    dispatch({
      type: types.LOADING_SHEETS,
      payload: axios.get('http://potencia.vincoorbisdev.com/wp-json/potencia/v1/files')
    });
  };
}
