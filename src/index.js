import 'babel-polyfill';

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import App from './app/containers/App';
import Ficha from './app/containers/Ficha';
import Sheets from './app/containers/Sheets';
import configureStore from './app/store/configureStore';
import {Router, Route, browserHistory} from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';


injectTapEventPlugin();

const store = configureStore();

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}/>
      <Route path="/ficha" component={Ficha}/>
      <Route path="/sheets" component={Sheets}/>
    </Router>
  </Provider>,
  document.getElementById('root')
);
